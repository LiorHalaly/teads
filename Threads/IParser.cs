﻿namespace Threads
{
    public interface IParser
    {
        string[] GetCommandlineFilePath(string[] filePath);
        string ParseCommandLineArgs(string args);
    }
}