﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;

namespace Threads
{
    public class Execute : IExecute
    {
        IReadAndWrite readAndWrite;
        IParser parser;
        ITextMunipolation textMunipolation;
        public Execute(IReadAndWrite readAndWrite, IParser parser, ITextMunipolation textMunipolation)
        {
            this.readAndWrite = readAndWrite;
            this.textMunipolation = textMunipolation;
            this.parser = parser;
        }
        public int CountOfpopularWord { get; private set; }
        public string PopularWord { get; private set; }
        string finaleContent;
        private ConcurrentBag<string> wordInConcrit;
        private ConcurrentBag<string> carrentSting = new ConcurrentBag<string>();

        private void InitiateReadTreads(string[] filePath, string splitOption)
        {
            foreach (var item in filePath)
            {
                Thread t = new Thread(() => PipeReadindTheFiles(item));
                t.Start();
            }
        }

        private void InitiateWriteTreads()
        {
            Thread t = new Thread(() => readAndWrite.WriteToFile(finaleContent));
            t.Start();
        }

        private void PipeReadindTheFiles(string filePath)
        {
            var fileContent = readAndWrite.ReadText(filePath);
            wordInConcrit = SaveTextToConcurrentObject(fileContent, carrentSting);
        }

        private void PipeTextMunipolation(string sort = "-s", string splitOption = "-a")
        {
            Thread.Sleep(1000);
            var content = textMunipolation.ConvertListToString(wordInConcrit.ToList());
            var words = content.Split(' ');
            var ordered = textMunipolation.Sort(words, sort);
            PopularWord = textMunipolation.FindRecuentWord(ordered);
            CountOfpopularWord = textMunipolation.FindRecuentWordCount(ordered);
            var removeDuplicate = textMunipolation.RemoveRepeatedWord(ordered);
            var listWithSplitOption = textMunipolation.Split(removeDuplicate, splitOption);
            finaleContent = textMunipolation.ConvertListToString(listWithSplitOption);
        }

        private ConcurrentBag<string> SaveTextToConcurrentObject(string content, ConcurrentBag<string> vs)
        {
            vs.Add(content);
            return vs;
        }

        public void ExecuteTask(string[] filePath)
        {
            //get split and sort from command line
            Console.WriteLine("enter sort option:");
            var content = GetCommandlineArgs();
            var sort = parser.ParseCommandLineArgs(content);

            Console.WriteLine("enter split option:");
            content = GetCommandlineArgs();
            var split = parser.ParseCommandLineArgs(content);

            var filePathArrey = parser.GetCommandlineFilePath(filePath);
            InitiateReadTreads(filePathArrey, split);
            PipeTextMunipolation(sort, split);
            InitiateWriteTreads();
        }

        private string GetCommandlineArgs() => Console.ReadLine();
    }
}

