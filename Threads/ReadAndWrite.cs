﻿using System.Collections.Concurrent;
using System.IO;

namespace Threads
{
    public class ReadAndWrite : IReadAndWrite
    {
        public string ReadText(string filePath) => File.ReadAllText(filePath);

        public void WriteToFile(string content)
        {
            var path = @"c:\temp\MyTest.txt";

            File.WriteAllText(path, content);

        }
    }
}
