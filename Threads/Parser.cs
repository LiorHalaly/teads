﻿using System;
using System.Collections.Generic;

namespace Threads
{
    public class Parser : IParser
    {
        public string[] GetCommandlineFilePath(string[] filePath)
        {
            var paths = string.Join(" ", filePath);
            return paths.Split(',');
        }

        public string ParseCommandLineArgs(string args)
        {
            var content = args.Split(' ');
            return content[1];
        }
    }
}
