﻿namespace Threads
{
    public interface IExecute
    {
        int CountOfpopularWord { get; }
        string PopularWord { get; }

        void ExecuteTask(string[] filePath);
    }
}