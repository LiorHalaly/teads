﻿using System.Collections.Concurrent;
using System.Collections.Generic;

namespace Threads
{
    public interface ITextMunipolation
    {
        string ConvertListToString(IList<string> content);
        IList<string> FileContentParser(IList<string> list, string splitOption);
        string FindRecuentWord(string[] content);
        int FindRecuentWordCount(string[] content);
        IList<string> RemoveRepeatedWord(string[] content);
        string[] Sort(string[] words, string sortOrder = "-a");
        IList<string> Split(IList<string> list, string splitOption);
    }
}