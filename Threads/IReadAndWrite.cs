﻿namespace Threads
{
    public interface IReadAndWrite
    {
        string ReadText(string filePath);
        void WriteToFile(string content);
    }
}