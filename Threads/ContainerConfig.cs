﻿

using SimpleInjector;

namespace Threads
{
   public static class ContainerConfig
    {
        static Container container;
        public static Container ContainerConfigure()
        {
            container = new Container();
            container.Register<ITextMunipolation, TextMunipolation>();
            container.Register<IReadAndWrite, ReadAndWrite>();
            container.Register<IParser, Parser>();
            container.Register<IExecute, Execute>();
            container.Verify();
            return container;
        }
    }
}
