﻿namespace Threads
{
	public class Program
    {        
        public static void Main(string[] args)
        {
            IExecute execute;
            var container = ContainerConfig.ContainerConfigure();
            execute = container.GetInstance<IExecute>();
            //Execute execute = new Execute();
            execute.ExecuteTask(args);
        }

    } 
}

