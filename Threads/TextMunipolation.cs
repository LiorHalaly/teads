﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Threads
{
    public class TextMunipolation : ITextMunipolation
    {
        public string[] Sort(string[] words, string sortOrder = "-a")
        {
            if (sortOrder == "-s")
            {
                var orderd = words.OrderByDescending(i => i);
                return orderd.ToArray();
            }
            else
            {
                var orderd = words.OrderBy(i => i);
                return orderd.ToArray();
            }
        }

        public IList<string> Split(IList<string> list, string splitOption)
        {
            IList<string> parsedList = null;
            switch (splitOption)
            {
                case "-s":
                    parsedList = FileContentParser(list, splitOption);
                    break;
                case "-c":
                    parsedList = FileContentParser(list, splitOption);
                    break;
            }
            return parsedList;
        }

        public string ConvertListToString(IList<string> content)
        {
            string combindedString = string.Join(" ", content.ToArray());
            return combindedString;
        }

        public IList<string> FileContentParser(IList<string> list, string splitOption)
        {
            List<string> parseContent = new List<string>();
            foreach (var item in list)
            {
                if (splitOption == "-s")
                {
                    parseContent.Add($"{item} ");
                }

                if (splitOption == "-c")
                {
                    parseContent.Add($"{item},");
                }
            }
            return parseContent;
        }

        public string FindRecuentWord(string[] content)
        {
            var mostRepeatedWord = content.SelectMany(x => x.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries))
                             .GroupBy(x => x).OrderByDescending(x => x.Count())
                             .Select(x => x.Key).FirstOrDefault();

            return mostRepeatedWord;
        }

        public int FindRecuentWordCount(string[] content)
        {
            var results = content.GroupBy(x => x)
                              .Select(x => new { Count = x.Count(), Word = x.Key })
                              .OrderByDescending(x => x.Count);

            return results.First().Count;
        }

        public IList<string> RemoveRepeatedWord(string[] content)
        {
            var result = content.Distinct();
            return result.ToList();
        }
    }
}
